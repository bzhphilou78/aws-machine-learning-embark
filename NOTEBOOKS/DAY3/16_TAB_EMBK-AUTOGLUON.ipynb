{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display: top; background-image:url('https://d1.awsstatic.com/SageMaker/SageMaker%20reInvent%202020/ImgHead_Mohave.3b1df33baa5f71b53237bfa6911fb3adc4902c0e.png');background-position: center ;background-repeat: no-repeat;background-size: 100%; padding-top:20px;\">\n",
    "<div style=\"color:#FFFFFF; text-align: top;padding-bottom: 25px; padding-left: 15px\"><h1 style=\"font-weight: 900;\">AutoGluon</h1>\n",
    "</div>\n",
    "</div>\n",
    "<br>\n",
    "\n",
    "In this notebook, we use __AutoGluon__ to predict the __Outcome Type__ field of our Austin Animal dataset.\n",
    "\n",
    "1. <a href=\"#1\">Set up AutoGluon</a>\n",
    "2. <a href=\"#2\">Read the datasets</a>\n",
    "3. <a href=\"#3\">Train a classifier with AutoGluon</a>\n",
    "4. <a href=\"#4\">Model evaluation</a>\n",
    "5. <a href=\"#5\">Clean up model artifacts</a>\n",
    "\n",
    "__Austin Animal Center Dataset__:\n",
    "\n",
    "In this exercise, we are working with pet adoption data from __Austin Animal Center__. We have two datasets that cover intake and outcome of animals. Intake data is available from [here](https://data.austintexas.gov/Health-and-Community-Services/Austin-Animal-Center-Intakes/wter-evkm) and outcome is from [here](https://data.austintexas.gov/Health-and-Community-Services/Austin-Animal-Center-Outcomes/9t4d-g238). \n",
    "\n",
    "In order to work with a single table, we joined the intake and outcome tables using the \"Animal ID\" column and created a single __review.csv__ file. We also didn't consider animals with multiple entries to the facility to keep our dataset simple. If you want to see the original datasets and the merged data with multiple entries, they are available under `DATA/review` folder: Austin_Animal_Center_Intakes.csv, Austin_Animal_Center_Outcomes.csv and Austin_Animal_Center_Intakes_Outcomes.csv.\n",
    "\n",
    "__Dataset schema:__ \n",
    "- __Pet ID__ - Unique ID of pet\n",
    "- __Outcome Type__ - State of pet at the time of recording the outcome (0 = not placed, 1 = placed). This is the field to predict.\n",
    "- __Sex upon Outcome__ - Sex of pet at outcome\n",
    "- __Name__ - Name of pet \n",
    "- __Found Location__ - Found location of pet before entered the center\n",
    "- __Intake Type__ - Circumstances bringing the pet to the center\n",
    "- __Intake Condition__ - Health condition of pet when entered the center\n",
    "- __Pet Type__ - Type of pet\n",
    "- __Sex upon Intake__ - Sex of pet when entered the center\n",
    "- __Breed__ - Breed of pet \n",
    "- __Color__ - Color of pet \n",
    "- __Age upon Intake Days__ - Age of pet when entered the center (days)\n",
    "- __Age upon Outcome Days__ - Age of pet at outcome (days)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. <a name=\"1\">Set up AutoGluon</a>\n",
    "(<a href=\"#0\">Go to top</a>)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[AutoGluon](https://autogluon.mxnet.io/tutorials/tabular_prediction/index.html) implements many of the best practices that we have discussed in this class, and more!  In particular, it sets itself apart from other AutoML solutions by having excellent automated feature engineering that can handle text data and missing values without any hand-coded solutions (see their [paper](https://arxiv.org/abs/2003.06505) for details).  It is too new to be in an existing Sagemaker kernel, so let's install it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install -q --upgrade mxnet autogluon"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import needed libraries\n",
    "import pandas as pd\n",
    "from sklearn.model_selection import train_test_split\n",
    "from autogluon.tabular import TabularPredictor\n",
    "import autogluon.core as ag\n",
    "\n",
    "import sys\n",
    "\n",
    "if not sys.warnoptions:\n",
    "    import warnings\n",
    "\n",
    "    warnings.simplefilter(\"ignore\")\n",
    "    warnings.filterwarnings(\"ignore\", category=DeprecationWarning)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. <a name=\"2\">Read the dataset</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "Let's read the dataset into a dataframe, using Pandas, and split the dataset into train and test sets (AutoGluon will handle the validation itself)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv(\"../../DATA/austin-animal/Austin_Animal_dataset.csv\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_data, test_data = train_test_split(\n",
    "    df, test_size=0.1, shuffle=True, random_state=23\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. <a name=\"3\">Train a classifier with AutoGluon</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "We can run AutoGluon with a short snippet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 1000  # grab less data for a quick demo\n",
    "\n",
    "predictor = TabularPredictor(label=\"Outcome Type\").fit(train_data=train_data.sample(k))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interpreting the Training Output\n",
    "\n",
    "> After the training above finishes, examine the output and try to find the information below in the print out messages from AutoGluon. <br/>\n",
    "1. What is the shape of your training dataset?\n",
    "2. What kind of ML problem type does AutoGluon infer (classification, regression, ...)? Remember, you've never mentioned what kind of problem type it is; you only provided the label column.\n",
    "3. What does AutoGluon suggest in case it inferred the wrong problem type?\n",
    "4. Identify the kind of data preprocessing and feature engineering performed by AutoGluon.\n",
    "5. Find the basic statistics about your label in the print statements from AutoGluon.\n",
    "6. How many extra features were generated besides the originals in our dataset?\n",
    "7. What is the evaluation metric used?\n",
    "8. What does AutoGluon suggests to do if it inferred the wrong metric?\n",
    "9. What is the ration between train & validation dataset (try looking for `val` or `validation`)?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, re-run AutoGluon for a larger dataset and specify the `eval_metric` to be 'f1'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hyperparameter tuning\n",
    "With AutoGluon we can also tune hyperparameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set Neural Net options\n",
    "# Specifies non-default hyperparameter values for neural network models\n",
    "nn_options = {\n",
    "    # number of training epochs (controls training time of NN models)\n",
    "    \"num_epochs\": 10,\n",
    "    # learning rate used in training (real-valued hyperparameter searched on log-scale)\n",
    "    \"learning_rate\": ag.space.Real(1e-4, 1e-2, default=5e-4, log=True),\n",
    "    # activation function used in NN (categorical hyperparameter, default = first entry)\n",
    "    \"activation\": ag.space.Categorical(\"relu\", \"softrelu\", \"tanh\"),\n",
    "    # each choice for categorical hyperparameter 'layers' corresponds to list of sizes for each NN layer to use\n",
    "    \"layers\": ag.space.Categorical([100], [1000], [200, 100], [300, 200, 100]),\n",
    "    # dropout probability (real-valued hyperparameter)\n",
    "    \"dropout_prob\": ag.space.Real(0.0, 0.5, default=0.1),\n",
    "}\n",
    "\n",
    "# Set GBM options\n",
    "# Specifies non-default hyperparameter values for lightGBM gradient boosted trees\n",
    "gbm_options = {\n",
    "    # number of boosting rounds (controls training time of GBM models)\n",
    "    \"num_boost_round\": 100,\n",
    "    # number of leaves in trees (integer hyperparameter)\n",
    "    \"num_leaves\": ag.space.Int(lower=26, upper=66, default=36),\n",
    "}\n",
    "\n",
    "# Add both NN and GBM options into a hyperparameter dictionary\n",
    "# hyperparameters of each model type\n",
    "# When these keys are missing from the hyperparameters dict, no models of that type are trained\n",
    "hyperparameters = {\n",
    "    \"GBM\": gbm_options,\n",
    "    \"NN\": nn_options,\n",
    "}\n",
    "\n",
    "# To tune hyperparameters using Bayesian optimization to find best combination of params\n",
    "search_strategy = \"auto\"\n",
    "\n",
    "# Number of trials for hyperparameters\n",
    "num_trials = 5\n",
    "\n",
    "# HPO is not performed unless hyperparameter_tune_kwargs is specified\n",
    "hyperparameter_tune_kwargs = {\n",
    "    \"num_trials\": num_trials,\n",
    "    \"scheduler\": \"local\",\n",
    "    \"searcher\": search_strategy,\n",
    "}\n",
    "\n",
    "predictor = TabularPredictor(label=\"Outcome Type\", eval_metric=\"f1\").fit(\n",
    "    train_data,\n",
    "    # Set time limit to 20 minutes (can be adjusted)\n",
    "    time_limit=20 * 60,\n",
    "    hyperparameters=hyperparameters,\n",
    "    hyperparameter_tune_kwargs=hyperparameter_tune_kwargs,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. <a name=\"4\">Model evaluation</a>\n",
    "(<a href=\"#0\">Go to top</a>)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred = predictor.predict(test_data)\n",
    "predictor.evaluate_predictions(\n",
    "    y_true=test_data[\"Outcome Type\"], y_pred=y_pred, auxiliary_metrics=True\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "predictor.leaderboard(silent=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. <a name=\"5\">Clean up model artifacts</a>\n",
    "(<a href=\"#0\">Go to top</a>)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!rm -r AutogluonModels"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_mxnet_p36",
   "language": "python",
   "name": "conda_mxnet_p36"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
