{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## XGBoost Model - SageMaker\n",
    "\n",
    "In this notebook, you are asked to build & train a [__XGBoost Classifier__](https://github.com/dmlc/xgboost) to predict the __Outcome Type__ field of our Austin Animal dataset. You will also deploy this model once trained.\n",
    "\n",
    "\n",
    "1. <a href=\"#1\">Read the dataset</a>\n",
    "2. <a href=\"#2\">Exploratory Data Analysis</a>\n",
    "3. <a href=\"#3\">Select features to build the model</a>\n",
    "4. <a href=\"#4\">Training and test datasets</a>\n",
    "5. <a href=\"#5\">Data processing with ColumnTransformer</a>\n",
    "6. <a href=\"#6\">Train a classifier using SageMaker</a>\n",
    "7. <a href=\"#7\">Deploy and test the classifier using SageMaker</a>\n",
    "\n",
    "__Notes on [AWS SageMaker](https://docs.aws.amazon.com/sagemaker/index.html):__\n",
    "\n",
    "* Fully managed machine learning service, to quickly and easily get you started on building and training machine learning models - we have seen that already! Integrated Jupyter notebook instances, with easy access to data sources for exploration and analysis, abstract away many of the messy infrastructural details needed for hands-on ML - you don't have to manage servers, install libraries/dependencies, etc.!\n",
    "\n",
    "\n",
    "* Apart from building custom machine learning models in SageMaker notebooks, like we did so far, SageMaker also provides a few [built-in common machine learning algorithms](https://docs.aws.amazon.com/sagemaker/latest/dg/algos.html) (check \"SageMaker Examples\" from your SageMaker instance top menu for a complete updated list) that are optimized to run efficiently against extremely large data in a distributed environment. The trained model can then be directly deployed into a production-ready hosted environment for easy access at inference. \n",
    "\n",
    "__Austin Animal Center Dataset__:\n",
    "\n",
    "In this exercise, we are working with pet adoption data from __Austin Animal Center__. We have two datasets that cover intake and outcome of animals. Intake data is available from [here](https://data.austintexas.gov/Health-and-Community-Services/Austin-Animal-Center-Intakes/wter-evkm) and outcome is from [here](https://data.austintexas.gov/Health-and-Community-Services/Austin-Animal-Center-Outcomes/9t4d-g238). \n",
    "\n",
    "In order to work with a single table, we joined the intake and outcome tables using the \"Animal ID\" column and created a single __Austin_Animal_dataset.csv__ file. We also didn't consider animals with multiple entries to the facility to keep our dataset simple. If you want to see the original datasets and the merged data with multiple entries, they are available under `DATA/austin-animal` folder: Austin_Animal_Center_Intakes.csv, Austin_Animal_Center_Outcomes.csv and Austin_Animal_Center_Intakes_Outcomes.csv.\n",
    "\n",
    "__Dataset schema:__ \n",
    "- __Pet ID__ - Unique ID of pet\n",
    "- __Outcome Type__ - State of pet at the time of recording the outcome (0 = not placed, 1 = placed). This is the field to predict.\n",
    "- __Sex upon Outcome__ - Sex of pet at outcome\n",
    "- __Name__ - Name of pet \n",
    "- __Found Location__ - Found location of pet before entered the center\n",
    "- __Intake Type__ - Circumstances bringing the pet to the center\n",
    "- __Intake Condition__ - Health condition of pet when entered the center\n",
    "- __Pet Type__ - Type of pet\n",
    "- __Sex upon Intake__ - Sex of pet when entered the center\n",
    "- __Breed__ - Breed of pet \n",
    "- __Color__ - Color of pet \n",
    "- __Age upon Intake Days__ - Age of pet when entered the center (days)\n",
    "- __Age upon Outcome Days__ - Age of pet at outcome (days)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# # #Upgrade dependencies\n",
    "# !pip install --upgrade pip\n",
    "# !pip install --upgrade scikit-learn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. <a name=\"1\">Read the dataset</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "Let's read the dataset into a dataframe, using Pandas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "  \n",
    "df = pd.read_csv('../../DATA/austin-animal/Austin_Animal_dataset.csv')\n",
    "\n",
    "print('The shape of the dataset is:', df.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. <a name=\"2\">Exploratory Data Analysis</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "To perform exploratory data analysis common methods are `.head()`, `.info()` and `.describe()` in pandas. Try these statements with the dataframe that was loaded in earlier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another important step during EDA is to create statistics for the features and the labels separately. Let's separate model features and model target. To list all columns in the dataframe, you can use `.columns`.\n",
    "\n",
    "Careful if the dataframe has a large number of columns, as this will print a list of all column names. \n",
    "\n",
    "Once you've looked at the column names, it is good practice to create new variables to seperate the list of features from the label.\n",
    "\n",
    "You can use something like:\n",
    "\n",
    "```\n",
    "model_features = df.columns.drop('Target')\n",
    "model_target = 'Target'\n",
    "```\n",
    "\n",
    "Make sure to amend for the actual label name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once separated, you can explore the features set further, figuring out first what features are numerical or categorical. Beware that some integer-valued features could actually be categorical features, and some categorical features could be text features. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "numerical_features_all = df[model_features].select_dtypes(include=np.number).columns\n",
    "print('Numerical columns:',numerical_features_all)\n",
    "\n",
    "print('\\n')\n",
    "\n",
    "categorical_features_all = df[model_features].select_dtypes(include='object').columns\n",
    "print('Categorical columns:',categorical_features_all)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Target distribution\n",
    "\n",
    "As final step in the EDA, analyse the label/target to check if there is any skew."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "df[model_target].value_counts().plot.bar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the target plots we can identify whether or not we are dealing with imbalanced datasets - this means one result type is dominating the other one(s). \n",
    "\n",
    "Handling class imbalance is highly recommended, as the model performance can be greatly impacted. In particular the model may not work well for the infrequent classes, as there are not enough samples to learn patterns from, and so it would be hard for the classifier to identify and match those patterns. \n",
    "\n",
    "We might want to downsample the dominant class or upsample the rare the class, to help with learning its patterns. However, we should only fix the imbalance in training set, without changing the validation and test sets, as these should follow the original distribution. We will perform this task after train/test split. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. <a name=\"3\">Select features to build the model</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "Now it's time to make a final selection for model features. The goal is to build a classifier including __numerical, categorical__ and __text__ features but we don't have to use all features (especially if there is any correlation between features as this might have a negative effect on model performance). You can check for correlation with `.corr()`.\n",
    "\n",
    "In the section below, make a selection for each feature type and store the values in lists called `numerical_features`, `categorical_features`, `text_features`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The below statement updates the suggested model_features variable from above\n",
    "model_features = numerical_features + categorical_features + text_features"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Cleaning numerical features "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "for c in numerical_features:\n",
    "    print(c)\n",
    "    df[c].plot.hist(bins=5)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If for some histograms the values are heavily placed in the first bin, it is good to check for outliers, either checking the min-max values of those particular features and/or explore value ranges."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for c in numerical_features:\n",
    "    print(c)\n",
    "    print('min:', df[c].min(), 'max:', df[c].max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With `value_counts()` function, we can increase the number of histogram bins to 10 for more bins for a more refined view of the numerical features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for c in numerical_features: \n",
    "    print(c)\n",
    "    print(df[c].value_counts(bins=10, sort=False))\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If any outliers are identified as very likely wrong values, dropping them could improve the numerical values histograms, and later overall model performance. While a good rule of thumb is that anything not in the range of `(Q1 - 1.5 IQR) and (Q3 + 1.5 IQR)` is an outlier, other rules for removing 'outliers' should be considered as well. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The below code snippet checks for missing values for the numerical features in the example dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(df[numerical_features].isna().sum())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If any missing values, as a quick fix, we can apply mean imputation. This will replace the missing values with the mean value of the corresponding column.\n",
    "\n",
    "__Note__: The statistically correct way to perform mean/mode imputation before training an ML model is to compute the column-wise means on the training data only, and then use these values to impute missing data in both the train and test sets. So, you'll need to split your dataset first. Same goes for any other transformations we would like to apply to these numerical features, such as scaling. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Cleaning categorical features \n",
    "\n",
    "Let's also examine the categorical features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for c in categorical_features:\n",
    "    print(c)\n",
    "    print(df[c].unique())\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Note on boolean type features__: Some categories might be of boolean type, like __False__ and __True__. The booleans will raise errors when trying to encode the categoricals with sklearn encoders, none of which accept boolean types. If using pandas get_dummies to one-hot encode the categoricals, there's no need to convert the booleans. However, get_dummies is trickier to use with sklearn's Pipeline and GridSearch. \n",
    "\n",
    "One way to deal with the booleans is to convert them to strings, by using a mask and a map changing only the booleans. Another way to handle the booleans is to convert them to strings by changing the type of all categoricals to 'str'. This will also affect the nans, basically performing imputation of the nans with a 'nans' placeholder value! \n",
    "\n",
    "Applying the type conversion to both categoricals and text features, takes care of the nans in the text fields as well. In case other imputations are planned for the categoricals and/or test fields, notice that the masking shown above leaves the nans unchanged."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df[categorical_features + text_features] = df[categorical_features + text_features].astype('str')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a check on missing values for the categorical features (and text features here)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(df[categorical_features + text_features].isna().sum())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Converting categoricals into useful numerical features will also have to wait until after the train/test split."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Cleaning text features \n",
    "\n",
    "Also a good idea to look at the text fields. Text cleaning can be performed here, before train/test split, with less code. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for c in text_features:\n",
    "    print(c)\n",
    "    print(df[c].unique()[:10])\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We re-use the helper functions from the previous notebooks `cleanSentence`.\n",
    "\n",
    "__Warning__: The cleaning stage can take a few minutes, depending on how much text is there to process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Prepare cleaning functions\n",
    "import re, string\n",
    "import nltk\n",
    "from nltk.stem import SnowballStemmer\n",
    "\n",
    "stop_words = [\"a\", \"an\", \"the\", \"this\", \"that\", \"is\", \"it\", \"to\", \"and\"]\n",
    "\n",
    "stemmer = SnowballStemmer('english')\n",
    "\n",
    "def preProcessText(text):\n",
    "    # lowercase and strip leading/trailing white space\n",
    "    text = text.lower().strip()\n",
    "    \n",
    "    # remove HTML tags\n",
    "    text = re.compile('<.*?>').sub('', text)\n",
    "    \n",
    "    # remove punctuation\n",
    "    text = re.compile('[%s]' % re.escape(string.punctuation)).sub(' ', text)\n",
    "    \n",
    "    # remove extra white space\n",
    "    text = re.sub('\\s+', ' ', text)\n",
    "    \n",
    "    return text\n",
    "\n",
    "def lexiconProcess(text, stop_words, stemmer):\n",
    "    filtered_sentence = []\n",
    "    words = text.split(\" \")\n",
    "    for w in words:\n",
    "        if w not in stop_words:\n",
    "            filtered_sentence.append(stemmer.stem(w))\n",
    "    text = \" \".join(filtered_sentence)\n",
    "    \n",
    "    return text\n",
    "\n",
    "def cleanSentence(text, stop_words, stemmer):\n",
    "    return lexiconProcess(preProcessText(text), stop_words, stemmer)\n",
    "\n",
    "# Clean the text features\n",
    "for c in text_features:\n",
    "    print('Text cleaning: ', c)\n",
    "    df[c] = [cleanSentence(item, stop_words, stemmer) for item in df[c].values]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cleaned text features are ready to be vectorized after the train/test split."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Note__: More exploratory data analysis might reveal other important hidden atributes and/or relationships of the model features considered. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. <a name=\"4\">Training and test datasets</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "Go ahead and split the dataset into training (90%) and test (10%) subsets using sklearn's [__train_test_split()__](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html) function.\n",
    "\n",
    "Store the result of the split in `train_data` and `test_data`, e.g. `train_data, test_data = train_test_split(...)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "# Implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Target balancing\n",
    "\n",
    "During the EDA stage we noticed that the dataset was skewed towards one class. In the following section, we will blance the target by upsampling the underrepresented class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Training set shape:', train_data.shape)\n",
    "\n",
    "print('Class 0 samples in the training set:', sum(train_data[model_target] == 0))\n",
    "print('Class 1 samples in the training set:', sum(train_data[model_target] == 1))\n",
    "\n",
    "print('Class 0 samples in the test set:', sum(test_data[model_target] == 0))\n",
    "print('Class 1 samples in the test set:', sum(test_data[model_target] == 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Important note:__ We want to fix the imbalance only in training set. We shouldn't change the validation and test sets, as these should follow the original distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.utils import shuffle\n",
    "\n",
    "class_0_no = train_data[train_data[model_target] == 0]\n",
    "class_1_no = train_data[train_data[model_target] == 1]\n",
    "\n",
    "upsampled_class_0_no = class_0_no.sample(n=len(class_1_no), replace=True, random_state=42)\n",
    "\n",
    "train_data = pd.concat([class_1_no, upsampled_class_0_no])\n",
    "train_data = shuffle(train_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Training set shape:', train_data.shape)\n",
    "\n",
    "print('Class 1 samples in the training set:', sum(train_data[model_target] == 1))\n",
    "print('Class 0 samples in the training set:', sum(train_data[model_target] == 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. <a name=\"5\">Data processing with ColumnTransformer</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "For the data processing, we want to use ColumnTransformer. As shown in earlier notebooks, you first build separate pipelines to handle the numerical, categorical, and text features, and then combine them into a composite pipeline along with an estimator, a [XGBoost Classifier](https://github.com/dmlc/xgboost) here.\n",
    "\n",
    "   * For the numerical features pipeline, the __numerical_processor__ below, we impute missing values with the mean using sklearn's SimpleImputer, followed by a MinMaxScaler (don't have to scale features when using tree-based algorithms, but it's a good idea to see how to use more data transforms). If different processing is desired for different numerical features, different pipelines should be built - just like shown below for the two text features.\n",
    "   \n",
    "   \n",
    "   * In the categoricals pipeline, the __categorical_processor__ below, we impute with a placeholder value (no effect here as we already encoded the 'nan's), and encode with sklearn's OneHotEncoder. If computing memory is an issue, it is a good idea to check categoricals' unique values, to get an estimate of many dummy features will be created by one-hot encoding. Note the __handle_unknown__ parameter that tells the encoder to ignore (rather than throw an error for) any unique value that might show in the validation/and or test set that was not present in the initial training set.\n",
    "  \n",
    "   \n",
    "   * And, finally, also with memory usage in mind, we build two more pipelines, one for each of our text features, trying different vocabulary sizes.\n",
    "   \n",
    "The selective preparations of the dataset features are then put together into a collective __ColumnTransformer__."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.impute import SimpleImputer\n",
    "from sklearn.preprocessing import OneHotEncoder, MinMaxScaler\n",
    "from sklearn.feature_extraction.text import CountVectorizer\n",
    "from sklearn.compose import ColumnTransformer\n",
    "from sklearn.pipeline import Pipeline\n",
    "\n",
    "\n",
    "# Implement code here.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. <a name=\"6\">Train a classifier</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "The goal of this notebook is to use Amazon SageMaker XGBoost algorithm to build the classifier. Below, the steps to train & deploy a SageMaker model will be explained. This includes uploading data to Amazon S3, training a model, and setting up an endpoint for online inference. \n",
    "\n",
    "### Set up the SageMaker environment\n",
    "\n",
    "Let's start by importing libraries we need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import boto3\n",
    "from os import path\n",
    "import sagemaker\n",
    "from sagemaker import get_execution_role\n",
    "from sagemaker.amazon.amazon_estimator import get_image_uri"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Upload datasets to Amazon S3\n",
    "\n",
    "Amazon SageMaker XGBoost can train on data in either a `CSV` or `recordIO-wrapped-protobuf` format. For this example, we stick with CSV.  \n",
    "\n",
    "So, let's write the data to Amazon S3 in recordio-protobuf format. We first create an io buffer wrapping the data, next we upload it to Amazon S3. Notice that the choice of bucket and prefix should change for different users and different datasets.\n",
    "\n",
    "Amazon SageMaker requires that a `CSV file does not have a header record and that the target variable is in the first column`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import boto3\n",
    "import sagemaker\n",
    "\n",
    "bucket = sagemaker.Session().default_bucket()\n",
    "prefix = 'XGBoost-demo'\n",
    "key = 'sample-data'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "SageMaker TrainingJobs work best with a train and validation data set (especially if you plan to use hyperparameter tuning), so implement a `train_test_split` with a 85/15 ratio below to create a validation subset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The test dataset remains the same as above. In the next step, we still want to leverage the ColumnTransfomer pipeline, albeit only the data processing part (without the model). To use this, we can call `data_processor.transform(...)`.\n",
    "\n",
    "The resulting dataframes need to be converted to arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get test data\n",
    "X_test = test_data[model_features]\n",
    "y_test = test_data[model_target]\n",
    "\n",
    "# Learn the transformation from training set and apply to validation and test\n",
    "X_train = data_preprocessor.fit_transform(X_train).toarray()\n",
    "X_val = data_preprocessor.transform(X_val).toarray()\n",
    "X_test = data_preprocessor.transform(X_test).toarray()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data is fully prepared now and can be uploaded to S3 as first step of the SageMaker Training Job. The first column of the file that is to be uploaded to S3 needs to contain the label."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "print(\"train_features shape = \", X_train.shape)\n",
    "print(\"train_labels shape = \", y_train.shape)\n",
    "\n",
    "train_df = pd.DataFrame(\n",
    "    np.concatenate((y_train.values.reshape(-1, 1), X_train), axis=1)\n",
    ")\n",
    "train_df.to_csv(\"/tmp/train.csv\", header=False, index=False)\n",
    "\n",
    "boto3.resource(\"s3\").Bucket(bucket).Object(\n",
    "    os.path.join(prefix, \"train\", key)\n",
    ").upload_file(\"/tmp/train.csv\")\n",
    "os.remove(\"/tmp/train.csv\")\n",
    "\n",
    "s3_train_data = \"s3://{}/{}/train/{}\".format(bucket, prefix, key)\n",
    "s3_train_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to provide validation data. This way we can get an test of the performance of the model from the training logs. In order to use this capability let's upload the validation data to Amazon S3 as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "print(\"validation_features shape = \", X_val.shape)\n",
    "print(\"validation_labels shape = \", y_val.shape)\n",
    "\n",
    "val_df = pd.DataFrame(np.concatenate((y_val.values.reshape(-1, 1), X_val), axis=1))\n",
    "val_df.to_csv(\"/tmp/validation.csv\", header=False, index=False)\n",
    "\n",
    "boto3.resource(\"s3\").Bucket(bucket).Object(\n",
    "    os.path.join(prefix, \"validation\", key)\n",
    ").upload_file(\"/tmp/validation.csv\")\n",
    "os.remove(\"/tmp/validation.csv\")\n",
    "\n",
    "s3_validation_data = \"s3://{}/{}/validation/{}\".format(bucket, prefix, key)\n",
    "s3_validation_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting the classifier\n",
    "\n",
    "We use the built-in Sagemaker [XGBoost](https://docs.aws.amazon.com/sagemaker/latest/dg/xgboost.html) below. \n",
    "\n",
    "In Amazon SageMaker, model training is done via an object called an __estimator__. When setting up the estimator we specify the location (in Amazon S3) of the training data, the path (again in Amazon S3) to the output directory where the model will be serialized, generic hyper-parameters such as the machine type to use during the training process, and kNN-specific hyper-parameters. Once the estimator is initialized, we can call its __.fit()__ method in order to do the actual training.\n",
    "\n",
    "* __Compute power:__ We will use `train_instance_count` and `train_instance_type` parameters. This example uses `ml.m4.xlarge` resource for training. The instance_type is the machine type that will host the model. We can change the instance type for our needs (for example GPUs for neural networks).\n",
    "* __Model type:__ `objective` is set to __`binary:logistic`__, as we have a classification problem here.\n",
    "* __Hyperparameters:__ We update some of the parameters __`max_depth`__, __`num_rounds`__ and __`subsample`__. The __`subsample`__ parameter determines what percentage of randomly sampled points from the data set should be used for building the individual trees. Using all the data points is tempting and definitely can't hurt the quality of the outputs but is often either infeasible or simply too costly. The __`num_round`__ parameter determines how many rounds to use for boosting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set an output path where the trained model will be saved\n",
    "output_path = \"s3://{}/{}/output\".format(bucket, prefix)\n",
    "\n",
    "# set required XGboost hyperparameters\n",
    "hyperparams = {\n",
    "    \"max_depth\": \"5\",\n",
    "    \"subsample\": \"0.7\",\n",
    "    \"verbosity\": \"1\",\n",
    "    \"objective\": \"binary:logistic\",\n",
    "    \"num_round\": \"50\",\n",
    "}\n",
    "\n",
    "region = boto3.Session().region_name\n",
    "\n",
    "# Create a container with XGBoost\n",
    "container = sagemaker.image_uris.retrieve(\"xgboost\", region, \"latest\")\n",
    "\n",
    "# Call the XGBoost estimator object\n",
    "XGBoost_estimator = sagemaker.estimator.Estimator(\n",
    "    container,\n",
    "    get_execution_role(),\n",
    "    hyperparameters=hyperparams,\n",
    "    instance_count=1,\n",
    "    instance_type=\"ml.m5.2xlarge\",\n",
    "    output_path=output_path,\n",
    "    sagemaker_session=sagemaker.Session(),\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display(container)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because we’re training with the CSV file format, we’ll create TrainingInputs that our training function can use as a pointer to the files in S3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.inputs import TrainingInput\n",
    "\n",
    "# define the types of files\n",
    "c_type = \"csv\"\n",
    "\n",
    "train_input = TrainingInput(s3_train_data, content_type=c_type)\n",
    "validation_input = TrainingInput(s3_validation_data, content_type=c_type)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "XGBoost_estimator.fit({'train': train_input, 'validation': validation_input})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tuning the classifier\n",
    "In the section below, we will explore the SageMaker hyperparameter tuning job ([documentation](https://sagemaker.readthedocs.io/en/stable/api/training/tuner.html)). Hyperparameter tuning refers to repeated training of a model with different parameter values. For each set of parameters, the model performance is recorded and eventually the best model will be preserved. There exist different strategies on how to efficiently try parameter values.\n",
    "\n",
    "To tune the model, parameter ranges are required. These can be passed in as integer, continuous ranges or a list of categorical values.\n",
    "\n",
    "The second component of the hyperparameter tuning is the objective setting. To tune, we require a goal. That can be accuracy, AUC or a balanced score such as F1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.tuner import IntegerParameter, CategoricalParameter, ContinuousParameter\n",
    "\n",
    "hyper_ranges = {\n",
    "    \"max_depth\": IntegerParameter(5, 10),\n",
    "    \"eta\": ContinuousParameter(1e-4, 1e-2),\n",
    "    \"num_round\": IntegerParameter(20, 30)\n",
    "}\n",
    "\n",
    "objective_metric_name = 'validation:auc'\n",
    "objective_type = 'Maximize'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The tuning ranges need to be combined with the model itself (we called it `XGBoost_estimator` above). For possible values check [here](https://docs.aws.amazon.com/sagemaker/latest/dg/xgboost-tuning.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.tuner import HyperparameterTuner\n",
    "\n",
    "tuner = HyperparameterTuner(XGBoost_estimator,\n",
    "                            objective_metric_name,\n",
    "                            hyper_ranges,\n",
    "                            max_jobs=10,\n",
    "                            max_parallel_jobs=1,\n",
    "                            strategy='Random',\n",
    "                            early_stopping_type='Auto',\n",
    "                            objective_type=objective_type)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The estimator needs to be fit with the hyperparameters from above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tuner.fit({'train': train_input, 'validation': validation_input})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 7. <a name=\"7\">Deploy and test the classifier using SageMaker</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "Now we test the best model with the best parameters on \"unseen\" data (our test data).\n",
    "\n",
    "Before that, let's first see how the model works on the training dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sagemaker.analytics.TrainingJobAnalytics(XGBoost_estimator._current_job_name, \n",
    "                                         metric_names = ['train:error']\n",
    "                                        ).dataframe() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If happy with the performance, it is time to deploy the model to another instance of our choice. We set up what is called an __endpoint__. An endpoint is a web service that given a request containing an unlabeled data point, or mini-batch of data points, returns a prediction(s). This allow us to use this model in production environment. \n",
    "\n",
    "Deployed endpoints can be used with other AWS Services such as Lambda and API Gateway. A nice walkthrough is available [here]( https://aws.amazon.com/blogs/machine-learning/call-an-amazon-sagemaker-model-endpoint-using-amazon-api-gateway-and-aws-lambda/) if you are interested.\n",
    "\n",
    "We use a `ml.t2.medium` instance here, but can also use other instance types such as:, `ml.c4.xlarge` etc. \n",
    "\n",
    "__Warning: This process takes about 10-11 minutes to complete.__\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "from sagemaker.serializers import CSVSerializer\n",
    "\n",
    "compiled_model = XGBoost_estimator\n",
    "compiled_model.name = \"deployed-xgboost-model\"\n",
    "endpoint = \"model-test-xgboost-%s\"%str(bucket.split('-')[-1])\n",
    "\n",
    "XGBoost_predictor = compiled_model.deploy(\n",
    "    initial_instance_count=1,\n",
    "    instance_type=\"ml.t2.medium\",\n",
    "    serializer=CSVSerializer(),\n",
    "    endpoint_name=endpoint \n",
    ")  # endpoint_name needs to be unique! Use AWS account number as part of name "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now make some predictions; let's test with 500 points:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred = XGBoost_predictor.predict(X_test[:500,:])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert predictions to array\n",
    "predictions = np.fromstring(y_pred.decode(\"utf-8\"), sep=\",\") "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As final task for this notebook, implement a classification accuracy report below that evaluates `predictions` vs `y_test` (the true labels of the test dataset). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__If you're ready to close this notebook, please run the cell below. This will remove the hosted endpoint you created and avoid any charges from a stray instance being left on.__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "XGBoost_predictor.delete_endpoint()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As optional exercise, feel free to implement another pre-built SageMaker model."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_python3",
   "language": "python",
   "name": "conda_python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
