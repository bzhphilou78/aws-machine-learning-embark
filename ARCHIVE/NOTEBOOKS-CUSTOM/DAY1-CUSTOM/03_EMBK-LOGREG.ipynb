{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Logistic Regression Hands-On\n",
    "\n",
    "In this notebook, we go over Logistic Regression to predict the __isPositive__ field of our Amazon Product Review dataset. \n",
    "\n",
    "1. <a href=\"#1\">Read the dataset</a>\n",
    "2. <a href=\"#2\">Exploratory Data Analysis</a>\n",
    "3. <a href=\"#3\">Select features to build the model</a>\n",
    "4. <a href=\"#4\">Data processing using Bag of Words</a>\n",
    "5. <a href=\"#5\">Train a classifier</a>\n",
    "\n",
    "*Find more details on the classical Linear Regression models [here](https://scikit-learn.org/stable/modules/classes.html#module-sklearn.linear_model).*\n",
    "\n",
    "__Amazon Product Review Dataset__:\n",
    "\n",
    "In this exercise, we are working with a product review dataset from Amazon. The raw data is available [here](https://nijianmo.github.io/amazon/index.html). You can find this dataset under `DATA/product-reviews`: Amazon_Reviews_Regression.csv and Amazon_Reviews_Classification.csv.\n",
    "\n",
    "__Dataset schema:__ \n",
    "* __reviewText__ - Text of the review\n",
    "* __summary__ - Summary of the review\n",
    "* __verified__ - Whether the purchase was verified (True or False)\n",
    "* __time__ - UNIX timestamp for the review\n",
    "* __rating__ - Rating of the review.\n",
    "* __log_votes__ - Logarithm-adjusted votes log(1+votes). \n",
    "* __isPositive__ - Whether the review is positive (1 or 0). This is the field to predict."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Read the dataset\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "We will use the Pandas library to read our dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved\n",
    "# SPDX-License-Identifier: MIT-0\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "\n",
    "df = pd.read_csv('../../DATA/product-reviews/Amazon_Reviews_Classification.csv')\n",
    "\n",
    "print('The shape of the dataset is:', df.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at the first five rows in the dataset. As you can see the __isPositive__ field only contains two categories. That's why we will build a classification model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Exploratory Data Analysis\n",
    "(<a href=\"#0\">Go to top</a>)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at the distribtion of the only numerical feature in the dataset (`log_votes`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Please complete code here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at the distribution of the categorical column in the dataset (`verified`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Please complete code here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Target distribution\n",
    "\n",
    "Please check the target distribution and assess whether it is skewed towards one category or another."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Please complete code here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Missing values\n",
    "Identify in the following section whether there are any missing values (how many missing values are there per column?)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Please complete code here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Imputation\n",
    "Let's fill-in the missing values for __reviewText__ below. We will just use the placeholder \"Missing\" here. We will focus on the __reviewText__ column and drop the __summary__ column later, so there is no need to fill the missing values for __summary__."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df[\"reviewText\"].fillna(\"Missing\", inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Depending on how many missing values there are in the dataset it might also be a good idea to drop the rows that contain missing values instead of imputing.\n",
    "\n",
    "Alternatively, there are also 'smart' imputers such as AWS Datawig [here](https://github.com/awslabs/datawig). Datawig uses Machine Learning to find good substitutes for missing values.\n",
    "\n",
    "It is also possible to impute with the most frequent or average value. To do this, sklearn has an Imputer class [here](https://scikit-learn.org/stable/modules/generated/sklearn.impute.SimpleImputer.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Selecting features to build the model\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "In this particular dataset, there are 2 text based columns which we want to prepare to use and 1 categorical and 1 numerical column each. Try to extract the weekday from the __time__ column using the below code snippet to create a new categorical column called __weekday__.\n",
    "\n",
    "\n",
    "```\n",
    "import datetime\n",
    "df['weekday'] = df['time'].apply(lambda x: datetime.datetime.fromtimestamp(x).weekday())\n",
    "df['weekday'] = df['weekday'].astype(str)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Please complete code here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['weekday'] = df['weekday'].astype(str)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also, make sure to drop the __summary__ column."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.drop('summary', axis = 1, inplace = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Cleaning text features \n",
    "\n",
    "Text cleaning can be performed here, before train/test split, with less code. The cleaning should happen after stop words have been removed as there will be fewer words to process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Install the library and functions\n",
    "import nltk\n",
    "\n",
    "nltk.download('punkt')\n",
    "nltk.download('stopwords')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will create the stop word removal and text cleaning processes below. NLTK library provides a list of common stop words. We will use the list, but remove some of the words from that list (because those words are actually useful to understand the sentiment in the sentence)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nltk, re\n",
    "from nltk.corpus import stopwords\n",
    "from nltk.stem import SnowballStemmer\n",
    "from nltk.tokenize import word_tokenize\n",
    "\n",
    "# Let's get a list of stop words from the NLTK library\n",
    "stop = stopwords.words('english')\n",
    "\n",
    "# These words are important for our problem. We don't want to remove them.\n",
    "excluding = ['against', 'not', 'don', \"don't\",'ain', 'aren', \"aren't\", 'couldn', \"couldn't\",\n",
    "             'didn', \"didn't\", 'doesn', \"doesn't\", 'hadn', \"hadn't\", 'hasn', \"hasn't\", \n",
    "             'haven', \"haven't\", 'isn', \"isn't\", 'mightn', \"mightn't\", 'mustn', \"mustn't\",\n",
    "             'needn', \"needn't\",'shouldn', \"shouldn't\", 'wasn', \"wasn't\", 'weren', \n",
    "             \"weren't\", 'won', \"won't\", 'wouldn', \"wouldn't\"]\n",
    "\n",
    "# New stop word list\n",
    "stop_words = [word for word in stop if word not in excluding]\n",
    "\n",
    "snow = SnowballStemmer('english')\n",
    "\n",
    "def process_text(texts): \n",
    "    final_text_list=[]\n",
    "    for sent in texts:\n",
    "        filtered_sentence=[]\n",
    "        \n",
    "        sent = sent.lower() # Lowercase \n",
    "        sent = sent.strip() # Remove leading/trailing whitespace\n",
    "        sent = re.sub('\\s+', ' ', sent) # Remove extra space and tabs\n",
    "        sent = re.compile('<.*?>').sub('', sent) # Remove HTML tags/markups:\n",
    "        \n",
    "        for w in word_tokenize(sent):\n",
    "            # We are applying some custom filtering here, feel free to try different things\n",
    "            # Check if it is not numeric and its length>2 and not in stop words\n",
    "            if(not w.isnumeric()) and (len(w)>2) and (w not in stop_words):  \n",
    "                # Stem and add to filtered list\n",
    "                filtered_sentence.append(snow.stem(w))\n",
    "        final_string = \" \".join(filtered_sentence) #final string of cleaned words\n",
    " \n",
    "        final_text_list.append(final_string)\n",
    "    \n",
    "    return final_text_list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df[\"reviewText\"] = process_text(df[\"reviewText\"].tolist()) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['verified'] = df['verified'].astype(str)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Data processing using Bag of Words (BoW)\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "Create a simple pipeline using column transformer. Make sure to create pre-processing pipelines for every data type (text, numerical, categorical)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import OneHotEncoder, MinMaxScaler\n",
    "from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.compose import ColumnTransformer\n",
    "\n",
    "\n",
    "### COLUMN_TRANSFORMER ###\n",
    "##########################\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "# Please complete code here.\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "# Combine all data preprocessors from above\n",
    "data_preprocessor = ColumnTransformer([\n",
    "    ('numerical_pre', numerical_processor, ['log_votes']),\n",
    "    ('categorical_pre', categorical_processor, ['verified', 'weekday']),\n",
    "    ('text_pre', text_processor, ['reviewText'][0]),\n",
    "                                    ]) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Train a classifier\n",
    "(<a href=\"#0\">Go to top</a>)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### LogisticRegression\n",
    "Let's fit __LogisticRegression__ from Sklearn library. Find more details on __LogisticRegression__ [here](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn import set_config\n",
    "set_config(display='diagram')\n",
    "\n",
    "X_train, y_train = df[[\"reviewText\", \"log_votes\", \"verified\", \"weekday\"]], df[\"isPositive\"].tolist()\n",
    "\n",
    "\n",
    "from sklearn.linear_model import LogisticRegression\n",
    "\n",
    "### PIPELINE ###\n",
    "################\n",
    "\n",
    "lr_pipeline = Pipeline([\n",
    "    ('data_preprocessing', data_preprocessor),\n",
    "    ('lrClassifier', LogisticRegression('none'))\n",
    "                    ])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Train the model using the `.fit()` method. Make sure to match the model name from above.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Please complete code here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"LogisticRegression model weights: \\n\", len(lr_pipeline[-1].coef_[0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Regularization\n",
    "\n",
    "Regularization is a technique used for reducing the number of features the model uses by adding an additional penalty term in the error function. It can help make the model simpler and generalize better to unseen data.\n",
    "\n",
    "There exist different regularization techniques and we will explore 2 of those in the sections below:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Ridge (L2 regularization)\n",
    "Let's now fit __LogisticRegression__ from Sklearn library with the so-called Ridge penalty term, and check how many model weights remain after the regularization is applied.\n",
    "\n",
    "Find more details on __Ridge__ [here](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html).\n",
    "\n",
    "To improve the performance of a LinearRegression model, __Ridge__ is tuning model complexity by adding a $L_2$ penalty score for complexity to the model cost function:\n",
    "\n",
    "$$\\text{C}_{\\text{regularized}}(\\textbf{w}) = \\text{C}(\\textbf{w}) +  {alpha}∗||\\textbf{w}||_2^2$$\n",
    "\n",
    "where $\\textbf{w}$ is the model weights vector, and $||\\textbf{w}||_2^2 = \\sum \\textbf{w}_i^2$.\n",
    "\n",
    "The strength of the regularization is controlled by the regularizer parameter, alpha: smaller value of $alpha$, weaker regularization; larger value of $alpha$, stronger regularization. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ridge_pipeline = Pipeline([\n",
    "    ('data_preprocessing', data_preprocessor),\n",
    "    ('lrClassifier', LogisticRegression(penalty='l2'))\n",
    "                    ])\n",
    "\n",
    "# Train the model\n",
    "ridge_pipeline.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"LogisticRegression model weights with Ridge and threshold at |0.5|: \\n\", len(ridge_pipeline[-1].coef_[0][abs(ridge_pipeline[-1].coef_[0]) > 0.5]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### LASSO ( L1 regularization)\n",
    "Let's also fit __LogisticRegression__ with Lasso penalty from Sklearn.\n",
    "\n",
    "Find more details on __Lasso__ [here](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html).\n",
    "\n",
    "__Lasso__ is tuning model complexity by adding a $L_1$ penalty score for complexity to the model cost function:\n",
    "\n",
    "$$\\text{C}_{\\text{regularized}}(\\textbf{w}) = \\text{C}(\\textbf{w}) +  alpha∗||\\textbf{w}||_1$$\n",
    "\n",
    "where $\\textbf{w}$ is the model weights vector, and $||\\textbf{w}||_1 = \\sum |\\textbf{w}_i|$. \n",
    "\n",
    "Again, the strength of the regularization is controlled by the regularizer parameter, $alpha$. Due to the geometry of $L_1$ norm, with __Lasso__, some of the weights will shrink all the way to 0, leading to sparsity - some of the features are not contributing to the model afterall!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lasso_pipeline = Pipeline([\n",
    "    ('data_preprocessing', data_preprocessor),\n",
    "    ('lrClassifier', LogisticRegression(penalty='l1', solver = 'liblinear'))\n",
    "                    ])\n",
    "\n",
    "# Train the model\n",
    "lasso_pipeline.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"LogisticRegression model weights with Lasso: \\n\", len(lasso_pipeline[-1].coef_[0][abs(lasso_pipeline[-1].coef_[0]) != 0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that Lasso only eliminated one model weight."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_python3",
   "language": "python",
   "name": "conda_python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
