{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## PyTorch: A Framework for Deep Learning\n",
    "\n",
    "This notebook dives into __how PyTorch stores and manipulates data__ using $n$-dimensional arrays, which are also called tensors. If you have worked with NumPy, the most widely-used scientific computing package in Python, then you will find this section familiar. No matter which framework you use, its tensor class is similar to NumPy's ndarray with a few killer features. First, __GPU is well-supported__ to accelerate the computation whereas NumPy only supports CPU computation. Second, the tensor class supports __automatic differentiation__. These properties make the tensor class suitable for deep learning. \n",
    "\n",
    "\n",
    "### Related Readings\n",
    "* [Chapter 2 of D2L](http://d2l.ai/chapter_preliminaries/index.html) contains some of the preliminary materials we covered here.  The material on automatic differentiation is covered elsewhere."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Ensure installation of specific libaries\n",
    "!pip install celluloid==0.2.0\n",
    "!pip install d2l==0.14.3\n",
    "!pip install IPython==7.16.1\n",
    "!pip install numpy==1.19.5\n",
    "!pip install matplotlib==3.3.4\n",
    "!pip install torch==1.8.1+cu101\n",
    "!pip install torchvision==0.9.1+cu101\n",
    "!pip install scikit-learn==0.24.1\n",
    "!pip install seaborn==0.11.1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Although it’s called PyTorch, we should import torch instead of pytorch\n",
    "import torch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Manipulation\n",
    "\n",
    "A tensor represents a (possibly multi-dimensional) array of numerical values. With one axis, a tensor corresponds (in math) to a vector. With two axes, a tensor corresponds to a matrix. Tensors with more than two axes do not have special mathematical names.\n",
    "\n",
    "To start, we can use arange to create a row vector x containing the first 12 integers starting with 0, though they are created as floats by default. Unless otherwise specified, a new tensor will be stored in main memory and designated for CPU-based computation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11])"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = torch.arange(12)\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can access a tensor’s shape (the length along each axis) by inspecting its shape property.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Size([12])"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can invoke the `reshape` function to transform our tensor, `x`,\n",
    "from a row vector with shape (12,) to a matrix with shape (3, 4)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[ 0,  1,  2,  3],\n",
       "        [ 4,  5,  6,  7],\n",
       "        [ 8,  9, 10, 11]])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x_reshaped = x.reshape(3, 4)\n",
    "x_reshaped"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If our target shape is a matrix with shape (height, width), then after we know the width, the height is given implicitly. Fortunately, tensors can automatically work out one dimension given the rest. We invoke this capability by placing `-1` for the dimension that we would like tensors to automatically infer. In our case, instead of calling `x.reshape(3, 4)`, we could have equivalently called `x.reshape(-1, 4)` or `x.reshape(3, -1)`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Typically, we will want our matrices initialized either with zeros, ones, some other constants, or numbers randomly sampled from a specific distribution. We can create a tensor representing a tensor with all elements set to 0 and a shape of (2, 3, 4) as follows:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[0., 0., 0., 0.],\n",
       "         [0., 0., 0., 0.],\n",
       "         [0., 0., 0., 0.]],\n",
       "\n",
       "        [[0., 0., 0., 0.],\n",
       "         [0., 0., 0., 0.],\n",
       "         [0., 0., 0., 0.]]])"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "zeros_tensor = torch.zeros((2, 3, 4))\n",
    "zeros_tensor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, we can create tensors with each element set to 1 as follows:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[[1., 1., 1., 1.],\n",
       "         [1., 1., 1., 1.],\n",
       "         [1., 1., 1., 1.]],\n",
       "\n",
       "        [[1., 1., 1., 1.],\n",
       "         [1., 1., 1., 1.],\n",
       "         [1., 1., 1., 1.]]])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ones_tensor = torch.ones((2, 3, 4))\n",
    "ones_tensor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often, we want to randomly sample the values for each element in a tensor from some probability distribution. For example, when we construct arrays to serve as parameters in a neural network, we will typically initialize their values randomly. The following snippet creates a tensor with shape (3, 4). Each of its elements is randomly sampled from a standard Gaussian (normal) distribution with a mean of 0 and a standard deviation of 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[-0.2214, -0.6073,  0.2648, -0.7102],\n",
       "        [-0.9860,  1.4167, -2.1402, -1.3381],\n",
       "        [ 0.0731, -0.2431,  0.1478,  0.6041]])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = torch.randn(3, 4)\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can access the dimension of a tensor by the `.shape` attribute. We can also query its size with `numel()`, which is equal to the product of the components of the shape. In addition, `.dtype` tells the data type of the stored values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(torch.Size([3, 4]), 12, torch.float32)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x.shape, x.numel(), x.dtype"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Indexing and Slicing\n",
    "\n",
    "Just as in any other Python array, elements in a tensor can be accessed by index."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(tensor([[-0.2214, -0.6073,  0.2648, -0.7102],\n",
       "         [-0.9860,  1.4167, -2.1402, -1.3381],\n",
       "         [ 0.0731, -0.2431,  0.1478,  0.6041]]),\n",
       " tensor([ 0.0731, -0.2431,  0.1478,  0.6041]),\n",
       " tensor([[-0.9860,  1.4167, -2.1402, -1.3381],\n",
       "         [ 0.0731, -0.2431,  0.1478,  0.6041]]))"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x, x[-1], x[1:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Beyond reading, we can also write elements of a matrix by specifying indices.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[-0.2214, -0.6073,  0.2648, -0.7102],\n",
       "        [-0.9860,  1.4167,  9.0000, -1.3381],\n",
       "        [ 0.0731, -0.2431,  0.1478,  0.6041]])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x[1, 2] = 9\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Operations \n",
    "\n",
    "The common standard arithmetic operators\n",
    "(`+`, `-`, `*`, `/`, and `**`)\n",
    "have all been *lifted* to elementwise operations\n",
    "for any identically-shaped tensors of arbitrary shape.\n",
    "We can call elementwise operations on any two tensors of the same shape.\n",
    "In the following example, we use commas to formulate a 5-element tuple,\n",
    "where each element is the result of an elementwise operation.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(tensor([ 3.,  4.,  6., 10.]),\n",
       " tensor([-1.,  0.,  2.,  6.]),\n",
       " tensor([ 2.,  4.,  8., 16.]),\n",
       " tensor([0.5000, 1.0000, 2.0000, 4.0000]),\n",
       " tensor([ 1.,  4., 16., 64.]))"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = torch.tensor([1.0, 2.0, 4.0, 8.0])\n",
    "y = torch.tensor([2.0, 2.0, 2.0, 2.0])\n",
    "\n",
    "# Sum, difference, element-wise multiplication, division, exponentiation (operator **)\n",
    "x + y, x - y, x * y, x / y, x ** y  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Conversion to Other Python Objects\n",
    "\n",
    "Converting to a NumPy tensor, or vice versa, is easy.\n",
    "The converted result does not share memory.\n",
    "This minor inconvenience is actually quite important:\n",
    "when you perform operations on the CPU or on GPUs,\n",
    "you do not want to halt computation, waiting to see\n",
    "whether the NumPy package of Python might want to be doing something else\n",
    "with the same chunk of memory.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(numpy.ndarray, torch.Tensor)"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A = x.numpy()\n",
    "B = torch.tensor(A)\n",
    "\n",
    "type(A), type(B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To convert a size-1 tensor to a Python scalar,\n",
    "we can invoke the `item` function or Python's built-in functions.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(tensor([3.5000]), 3.5, 3.5, 3)"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = torch.tensor([3.5])\n",
    "\n",
    "a, a.item(), float(a), int(a)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_pytorch_latest_p36",
   "language": "python",
   "name": "conda_pytorch_latest_p36"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
