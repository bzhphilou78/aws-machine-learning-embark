{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Word Vectors - Hands-On\n",
    "Word vectors refers to a family of related techniques, first gaining popularity via ```Word2Vec``` which associates an $n$-dimensional vector (normally $n$ is in the range of $50$ to $500$) to every word in the target language.\n",
    "\n",
    "We will first load a batch of word vectors known as [ConceptNet Numberbatch](https://github.com/commonsense/conceptnet-numberbatch), which have been found to have excellent performance while reducing issues of [learning human bias](https://gist.github.com/rspeer/ef750e7e407e04894cb3b78a82d66aed).  Learning how to construct these word vectors is a bit beyond the scope of what we can cover in this notebook, but [this two-part blog post provides an excellent introduction](http://mccormickml.com/2016/04/19/word2vec-tutorial-the-skip-gram-model/).\n",
    "\n",
    "__How to get the most from this notebook__:\n",
    "This notebook builds out a solution in a step by step manner making it clear where data is being used and what tools are useful for exploration.  After every code block, I encourage you to explore your own problem from beginning to end using these tools."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved\n",
    "# SPDX-License-Identifier: MIT-0\n",
    "\n",
    "! wget https://conceptnet.s3.amazonaws.com/downloads/2017/numberbatch/numberbatch-en-17.06.txt.gz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! rm -rf numberbatch-en-17.06.txt\n",
    "! gzip -d numberbatch-en-17.06.txt.gz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load our libraries\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "\n",
    "# Load word vectors\n",
    "words = pd.read_csv('numberbatch-en-17.06.txt',\n",
    "                    sep=\" \",\n",
    "                    index_col=0,\n",
    "                    header=None,\n",
    "                    skiprows=[0]).transpose()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code snippet above loads our wordvectors.  The Pandas table ```words``` allows us to perform lookups like ```words['house']``` to get the associated vectors.  Let's just print one out for reference."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(words['house'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try and print the word vectors for other words. Compare the vectors of similar words such as 'forest' & 'tree' vs 'painting' & 'pizza'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Such a vector is not particularly informative to us since it is not organized in a humanly readable way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computing Distances\n",
    "As part of our \"manipulation primitives\", we often need to be able to compute distances between vectors associated to words.  So we start by writing a little snippet that lets us do so.  ```numpy``` makes this fairly easy to do.  Remember that small distances correspond to similar words, so lets check this by going through and writing a little code that takes three words and tells you if the first word is closer to the second than the third."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define the distance between two words\n",
    "def dist(w1,w2) :\n",
    "    return np.linalg.norm(words[w1] - words[w2])\n",
    "    \n",
    "# Say if w1 is closer to w2 than w3\n",
    "def distCompare(w1, w2, w3) :\n",
    "    d2 = dist(w1,w2)\n",
    "    d3 = dist(w1,w3)\n",
    "    if d2 < d3 :\n",
    "        print(\"{} is closer to {} than {}\".format(w1,w2,w3))\n",
    "    else :\n",
    "        print(\"{} is closer to {} than {}\".format(w1,w3,w2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the `distCompare` function to compare the following triplets:\n",
    "- 'orb','ball','hockey'\n",
    "- 'picked','lifted','play'\n",
    "- 'pink','red','blue'\n",
    "Do you agree with the results?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that it mostly agrees with what we anticipated.  If you continued to ask more questions, you'd find some things that disagree with what you would expect (for instance, it believes that ```'maroon'``` is closer to ```'blue'``` than ```'red'```), but on the whole, you'll find it agrees with the intuition that similar words should be close to one another. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear Structure of Word Vectors (Subtraction)\n",
    "\n",
    "If word vectors only put similar words next to one another, they would have never garnered the interest that they have obtained from the community.  Indeed they actually contain subtle and nuanced understanding of the meanings of words.  It will take a while to explore what this means, but the mantra that we should now internalize is \"relationships = directions\" which is to say that words that share a similar relationship, will be separated from one another in the same direction.\n",
    "\n",
    "As we saw, vector subtraction lets us examine this.  However, since vector subtraction is as simple as\n",
    "```python\n",
    "diff = v - w\n",
    "```\n",
    "there is not much to look at here.\n",
    "\n",
    "## Reverse Lookup\n",
    "\n",
    "Reverse lookup will allow us to probe the finer structure of word vectors.  In particular, we will now create a reverse lookup routine that finds the $k$ closest words to a given vector.  As the straight-forward implementation will be too slow (looping over every element of ```words```) we will provide you with a ```numpy``` implementation which will be fast enough for our needs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# drop things containing underscores (these are compound terms like \"young_man\" that our code will not use) and convert to matrix format for faster computation\n",
    "labels  = words.columns.values.tolist()\n",
    "labels = np.array([w for w in labels if isinstance(w,str) and w.isalpha()])\n",
    "wordsMatrix = words[labels].values\n",
    "\n",
    "# snipped to find the closest word (or vector)\n",
    "def find_closest_word(v, k = 1):\n",
    "    if type(v) == type('str'):\n",
    "        v = words[v]\n",
    "    diff = wordsMatrix - v.values.reshape(-1,1)\n",
    "    delta = np.linalg.norm(diff, axis=0)\n",
    "    return labels[np.argsort(delta)[:k]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test the `find_closest_word` function with the 50 closest words to blue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should now see lLots of blue related words, and then words related to other colors.  Many of them, like ```'tetronerythrin'``` actually relate to specific pigments."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Application: Analogies\n",
    "\n",
    "Let us suppose that we have four associated word vectors $v_{man}$, $v_{woman}$, $v_{boy}$, and $v_{girl}$.  If we believe the idea that \"relationship = direction\" then, this becomes a vector relationship, where the vector that takes us from $v_{man}$ to $v_{woman}$ should be the same as the vector that takes us from $v_{boy}$ to $v_{girl}$.  Recalling that vector subtraction is what gives us such a direction, this becomes\n",
    "$$\n",
    "v_{woman} - v_{man} \\approx v_{girl} - v_{boy}\n",
    "$$\n",
    "\n",
    "Suppose you now wanted to solve an analogy using this idea.  Say we were just given ```man:woman::boy:?``` and we wanted to find the question mark.  The expression above can be rearranged by adding $v_{boy}$ to both sides to yield\n",
    "$$\n",
    "v_{?} \\approx v_{woman} - v_{man} + v_{boy}\n",
    "$$\n",
    "Thus the word we are looking for should hopefully be the word whose associated vector is closest to $v_{woman} - v_{man} + v_{boy}$.  Let's see how this works out in code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A little snippet for analogies\n",
    "def analogy(w1,w2,w3, k = 1) : \n",
    "    listPoss = find_closest_word(words[w2] - words[w1] + words[w3], k)\n",
    "    print(\"{} : {} :: {} : {}\".format(w1,w2,w3,listPoss))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the `analogy` function to create associated vectors. Find the 2 closest examples for:\n",
    "- 'man','woman','boy'\n",
    "- 'short', 'tall', 'shortest'\n",
    "- 'seattle','washington','minneapolis'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All in all, this should have worked fantastically!  The first one will indeed show that ```man : woman :: boy : girl``` as the most likely choice. The second will state that ```short : tall :: shortest : tallest``` is the most likely case indicating that it understands how to turn words into superlatives (not just a simple relationship of size).  The third one indicates it understands what the largest cities in Washington and Minnesota are and can retrieve that information if needed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualization (PCA)\n",
    "\n",
    "Let's implement our last little primitive: the ability to automatically visualize what a collection of vectors is doing by projecting it onto the best possible pair of directions.  We will use [sklearn](http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html) to do this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from sklearn.decomposition import PCA\n",
    "\n",
    "def pcaPlot(word_list) :\n",
    "    # fetch list of word vectors\n",
    "    vecs = [words[x] for x in word_list]\n",
    "    \n",
    "    #reduce dimensions\n",
    "    model = PCA(n_components = 2)\n",
    "    reduced = model.fit_transform(vecs)\n",
    "    xc = [v[0] for v in reduced]\n",
    "    yc = [v[1] for v in reduced]\n",
    "    \n",
    "    # plot them\n",
    "    plt.figure(figsize=(10,10))\n",
    "    plt.scatter(xc, yc)\n",
    "\n",
    "    # label the plot\n",
    "    for i, word in enumerate(word_list) :\n",
    "    \tplt.annotate(word, xy=(xc[i], yc[i]+0.01), fontsize=12)\n",
    "    plt.show()\n",
    "\n",
    "pcaPlot(['fast','faster','fastest','slow','slower','slowest'])\n",
    "pcaPlot(['bird', 'cat', 'squirrel', 'dog', 'fish', 'helicopter', 'airplane', 'car', 'submarine', 'whale'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Deleting notebook artifacts to free up memory\n",
    "! rm -rf numberbatch-en-17.06.txt.gz\n",
    "! rm numberbatch-en-17.06.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Word Embeddings with BERT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This section of the notebook provides a short example of installing GluonNLP, and then using a pretrained BERT model to encode a sentence.  This can be used as an encoding method for any other downstream learning algorithm, and is an excellent method to use during early stages of product development.  If the application seems sound, the model can be fine-tuned for additional performance.  Scripts to aid in this task can be found [here](https://gluon-nlp.mxnet.io/master/model_zoo/bert/index.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved\n",
    "# SPDX-License-Identifier: MIT-0\n",
    "\n",
    "# Load Relevant Libraries\n",
    "!pip install --upgrade pip\n",
    "!pip install --upgrade mxnet gluonnlp\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import gluonnlp as nlp\n",
    "import mxnet as mx\n",
    "\n",
    "# Load a Small BERT model\n",
    "model, vocab = nlp.model.get_model('bert_12_768_12', dataset_name='book_corpus_wiki_en_uncased', use_classifier=False, use_decoder=False);\n",
    "tokenizer = nlp.data.BERTTokenizer(vocab, lower=True);\n",
    "transform = nlp.data.BERTSentenceTransform(tokenizer, max_seq_length=512, pair=False, pad=False);\n",
    "\n",
    "# Transform Text\n",
    "sample = transform(['AWS Embark provides onboarding, training, and implementation support to launch your machine learning journey!']);\n",
    "words, valid_len, segments = mx.nd.array([sample[0]]), mx.nd.array([sample[1]]), mx.nd.array([sample[2]]);\n",
    "\n",
    "# Encode\n",
    "seq_encoding, cls_embedding = model(words, segments, valid_len);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step of using a transformer is to split the sentence into tokens for a vocabulary.  This is handled cleanly by Gluon, but we can look inside to see how it is split.  Notice that the model actually uses a subword vocabulary---where some words are split into constituant parts like \"onboarding\" becoming `'onboard'` and `'##ing'`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[vocab.to_tokens(int(w.asscalar())) for w in words[0]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now look at an embedding that can be used for downstream tasks like classification, called the `cls_embedding`.  The other term `seq_encoding` gives an encoding for each token in the sentence and can be used for tasks like machine translation or part-of-speach tagging."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# print the first few elements of cls_embedding\n",
    "cls_embedding[0][:10]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# print the first few elements of seq_embedding\n",
    "seq_encoding[0][:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Convert your own sample sentence to vectors for a sentiment classification task."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# implement code here"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_mxnet_p36",
   "language": "python",
   "name": "conda_mxnet_p36"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
