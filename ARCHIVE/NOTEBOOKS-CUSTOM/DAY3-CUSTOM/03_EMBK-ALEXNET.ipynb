{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fine-Tuning with Pre-trained AlexNet \n",
    "\n",
    "In this notebook, we use a pre-trained [AlexNet](https://d2l.ai/chapter_convolutional-modern/alexnet.html) on the [MINC](http://opensurfaces.cs.cornell.edu/publications/minc/)  dataset. Instead of building our own CNN from scratch, we want to use AlexNet. This is similar to what you would do in practice as the research community has spent significant time and testing on perfecting various CNN architectures.\n",
    "\n",
    "Therefore, the main topics of this particular notebook are:\n",
    "\n",
    "1. <a href=\"#1\">Loading and Transforming Dataset</a>      \n",
    "2. <a href=\"#2\">Fine-tuning Pretrained AlexNet</a>\n",
    "3. <a href=\"#3\">Testing and Visualizations</a>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let's update torch at least to v1.6.0 and d2l to v0.15.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ! pip install -U torch==1.6.0  # updating torch to at least v1.6\n",
    "# ! pip install -q d2l==0.15.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's import the necessary libraries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from d2l import torch as d2l\n",
    "import torch\n",
    "import torchvision\n",
    "from torch import nn\n",
    "from torchvision import transforms\n",
    "import numpy as np\n",
    "\n",
    "from torch.utils.data import TensorDataset, DataLoader"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. <a name=\"1\">Loading and Transforming Dataset</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "To load the dataset properly, we need to massage the image data a bit by some `transforms` functions. PyTorch provides a full list of [transforms functions](https://pytorch.org/docs/stable/torchvision/transforms.html) to enable a wide variety of data augmentation. \n",
    "\n",
    "We will process some simple data transformations in this example. First, we load the image data and resize it to the given size (224,224). Next, we convert the image tensor of shape (H x W x C) in the range [0, 255] to a float32 tensor of shape (C x H x W) in the range (0, 1) using the `ToTensor` class. Last, we normalize the tensor of shape (C x H x W) with its mean and standard deviation by `Normalize`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transform_train = transforms.Compose([\n",
    "    transforms.Resize(224),\n",
    "    transforms.ToTensor(),\n",
    "    transforms.Normalize(mean=(0,0,0), std=(1,1,1))\n",
    "])\n",
    "\n",
    "transform_test = transforms.Compose([\n",
    "    transforms.Resize(224),\n",
    "    transforms.ToTensor(),\n",
    "    transforms.Normalize(mean=(0,0,0), std=(1,1,1))\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now apply the predefined transform functions and load the train, validation and test sets.\n",
    "\n",
    "In practice, reading data can be a significant performance bottleneck, especially when our model is simple or when our computer is fast. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path = '../../DATA/minc-2500'\n",
    "train_path = os.path.join(path, 'train')\n",
    "val_path = os.path.join(path, 'val')\n",
    "test_path = os.path.join(path, 'test')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To make our life easier when reading from the datasets, we use a `DataLoader` of Gluon, which reads a minibatch of data with size `batch_size` each time.\n",
    "\n",
    "Go ahead, set up the `DataLoader` (for train, test and validation respectively) and make sure it points to the correct path. As the `minc-2500` folder contains sub-folders, ensure that you are using the `ImageFolder` import option as well as the `transforms.Compose()` object from above:\n",
    "```\n",
    "DataLoader(\n",
    "    torchvision.datasets.ImageFolder(path, transform=transform_train)\n",
    "    , batch_size=batch_size\n",
    "    , shuffle=True\n",
    "    )\n",
    "```\n",
    "For batch size chose 16."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. <a name=\"2\">Fine-tuning Pretrained AlexNet</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "To fine-tune a pretrained model, we need the following 4 steps:\n",
    "1. Define a neural network **finetune_net** with AlexNet architecture, and later reshape for given number of output classes. Note that for `torchvision.models.alexnet` the default parameter `pretrained` is False, which means it will only return us an AlexNet architecture rather than an AlexNet architecture with the pretrained weights.\n",
    "\n",
    "1. Initialize the **finetune_net** with [Xavier initialization](https://d2l.ai/chapter_multilayer-perceptrons/numerical-stability-and-init.html#xavier-initialization) to make sure our random initialized weights are neither too small nor too huge.\n",
    "\n",
    "1. Define another neural network, **pretrained_net**, and load the pretrained AlexNet model (which was trained on ImageNet) on it. Here, by specifying ``pretrained=True``, it will automatically download the model from torchvision model zoo if necessary. For more pretrained models, please refer to [torchvision Models](https://pytorch.org/docs/stable/torchvision/models.html).\n",
    "\n",
    "1. Transfer the trained weights (except last layer) from **pretrained_net** to **finetune_net**, and output **finetune_net**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "feature_extract = True\n",
    "\n",
    "def set_parameter_requires_grad(model, feature_extracting):\n",
    "    if feature_extracting:\n",
    "        for param in model.parameters():\n",
    "            param.requires_grad = False\n",
    "\n",
    "def FineTuneAlexnet(classes, device):\n",
    "    '''\n",
    "    classes: number of the output classes \n",
    "    device: training context (CPU or GPU)\n",
    "    '''\n",
    "    finetune_net = torchvision.models.alexnet(pretrained=True).to(device)\n",
    "    set_parameter_requires_grad(finetune_net, feature_extract)\n",
    "    num_ftrs = finetune_net.classifier[6].in_features\n",
    "    finetune_net.classifier[6] = nn.Linear(num_ftrs, classes)\n",
    "    \n",
    "    return finetune_net"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To avoid updating an already trained model, make sure to delete `net` first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    del net\n",
    "except:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create a `net` using the `FineTuneAlexnet` on available GPUs (or CPUs) by defining the training context `ctx`. Since the MINC dataset has 6 categories, the output classes will be 6."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "device = d2l.try_gpu() # Create neural net on CPU or GPU depending on your training instances\n",
    "num_outputs = 6        # 6 output classes\n",
    "net = FineTuneAlexnet(num_outputs, device)\n",
    "\n",
    "net"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we set up the hyperparameters for training, such as the learning rate of optimization algorithms. With the defined learning rate, we are able to create an `Optimizer` to infer the neural network \"how to optimize its weights\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "learning_rate = 0.001\n",
    "\n",
    "#  We will only update the parameters that we have just initialized,\n",
    "#  i.e. the parameters with requires_grad is True.\n",
    "params_to_update = net.parameters()\n",
    "print(\"Params to learn:\")\n",
    "if feature_extract:\n",
    "    params_to_update = []\n",
    "    for name,param in net.named_parameters():\n",
    "        if param.requires_grad == True:\n",
    "            params_to_update.append(param)\n",
    "            print(\"\\t\",name)\n",
    "else:\n",
    "    for name,param in model_ft.named_parameters():\n",
    "        if param.requires_grad == True:\n",
    "            print(\"\\t\",name)\n",
    "\n",
    "optimizer = torch.optim.SGD(params_to_update, lr=learning_rate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Besides, we need to specify the loss function. Since this problem is a multiclass classification task, we will use cross entropy loss as our loss funciton. You can look up the correct method [here](https://pytorch.org/docs/stable/nn.html#loss-functions). Implement the loss function below and call it `loss_criterion`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our network is almost ready to be finetuned! One last thing before the finetuning is to define the `accuracy` function for evulating our model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def finetune_accuracy(output, label):\n",
    "    # output: (batch, num_output) float32 tensor\n",
    "    # label: (batch, ) int32 tensor\n",
    "    return (output.argmax(axis=1) == label.float()).float().mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it's the training time! Starting with the outer loop, we will have 10 epochs (10 full pass through our dataset). Within the inner loop, we yield each mini-batch from the `train_loader`, and update the weights based on the average statistics of this mini-batch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# implement code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you would like to save the trained model, no matter using it for inference or to retrain it later. You can call `save_parameters` function to save the model architecture and its weights."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "torch.save(net.state_dict(), \"my_model\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the training and validation loss below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implement code here\n",
    "\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "\n",
    "plt.plot(train_losses, label=\"Training Loss\")\n",
    "plt.plot(val_losses, label=\"Validation Loss\")\n",
    "plt.title(\"Loss values\")\n",
    "plt.xlabel(\"Epoch\")\n",
    "plt.ylabel(\"Loss\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. <a name=\"3\">Testing and Visualizations</a>\n",
    "(<a href=\"#0\">Go to top</a>)\n",
    "\n",
    "Let's validate our model predictions. Meanwhile, we use the `show_images` function and show the sample images and its prediction together."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def show_images(imgs, num_rows, num_cols, titles=None, scale=1.5):\n",
    "    \"\"\"Plot a list of images.\"\"\"\n",
    "    figsize = (num_cols * scale, num_rows * scale)\n",
    "    _, axes = d2l.plt.subplots(num_rows, num_cols, figsize=figsize)\n",
    "    axes = axes.flatten()\n",
    "    for i, (ax, img) in enumerate(zip(axes, imgs)):\n",
    "        ax.imshow(img.permute(1,2,0).numpy())\n",
    "        ax.axes.get_xaxis().set_visible(False)\n",
    "        ax.axes.get_yaxis().set_visible(False)\n",
    "        if titles:\n",
    "            ax.set_title(titles[i])\n",
    "    return axes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_dataset = torchvision.datasets.ImageFolder(test_path, transform=transform_test)\n",
    "random_test_loader = DataLoader(test_dataset, batch_size=2*8, shuffle=True)\n",
    "\n",
    "net.eval()\n",
    "for data, label in random_test_loader:\n",
    "    show_images(data, 2, 8);\n",
    "    data = data.to(device)\n",
    "    pred = net(data)\n",
    "    print(pred.argmax(axis=1))\n",
    "    break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we calculate the performance of the pre-trained net over the full test data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True)\n",
    "\n",
    "test_predictions = []\n",
    "test_true = []\n",
    "for i, (data, label) in enumerate(test_loader):\n",
    "    test_preds = net(data.to(device))\n",
    "    test_predictions.extend([test_preds.detach().cpu().numpy().argmax(axis=1)])\n",
    "    test_true.extend([label.detach().cpu().numpy()])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Flatten the list (we're creating predictions in batches above) and create a confusion matrix to check the test performance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# implement code here"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_pytorch_latest_p36",
   "language": "python",
   "name": "conda_pytorch_latest_p36"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
